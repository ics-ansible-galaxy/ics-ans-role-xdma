import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_kernel_modules_present(host):
    cmd = host.run("/usr/sbin/modinfo xdma")
    assert "xdma.ko" in cmd.stdout
