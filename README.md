# ics-ans-role-xdma

Ansible role to install Xilinx DMA kernel module.

## Role Variables

See: [Default variables](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-xdma
```

## License

BSD 2-clause
